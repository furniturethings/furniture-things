Furniture & Things is a full service furniture, mattress, home decor, & gift store. Michelle & Eric Kimmel opened the store in January 2015.

We offer excellent quality furniture, mattresses, gifts and home decor that enrich our customers lives. Every customer gets personalized service.

Address: 1661 N State St, North Vernon, IN 47265, USA

Phone: 812-352-6099